﻿using UnityEngine;
using System.Collections;

public class Torch : MonoBehaviour {

	public AudioClip torch1;
	public AudioClip torch2;
	public AudioClip torch3;

	// Resolve torch circle collider collision
	void OnTriggerEnter2D(Collider2D col) {
		if (col.gameObject.tag == "Goblin") {
			col.gameObject.GetComponent<Goblin>().inTorchRange = true;
		}
	}
}
