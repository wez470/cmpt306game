﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Player : MonoBehaviour {
	//Joystick deadzone
	private const float ROT_DEAD_ZONE = 0.5f;
	
	//Player can only be so far from where the camera is centered. Used
	//to keep all players on the screen at the same time.
	private const int MAX_X_DIST_FROM_CAMERA = 33;
	private const int MAX_Y_DIST_FROM_CAMERA = 9;

	public GameObject LeftHand;
	public GameObject GrappleAndRope;
	public GameObject Shield;
	public GameObject RightHand;
	public GameObject PlayerSword;
	public GameObject Torch;
	public GameObject Rope;
	public float PlayerSpeed;
	public float GrappleSpeed;
	public float RotSpeed;
	public GameObject Grapple;
	public Transform GrappleSpawn;
	public float moveToGrappleSpeed;

	/* Used to store the players current deployed grapple if it exists
	   so we have control over it */
	private GameObject currGrapple = null;
	private	Animator anim;
	private PlayerMovement playerMovement;
	private float RtTimer = 0f;
	//Used to indicate whether the player should be following the grapple hook or not.
	private bool playerAnchored;
	//Deprecated. Used by our single member who still uses his PS4 controller for personal debugging.
	// private InputController input;
	//reference to the targeting system. Used to lock onto terrain, players, and enemies.
	private PlayerTargeting playerTarget;
	//string used to provide the layermask to the targetting system.
	//Each player needs to omit itself from this layermask so it can
	//shoot raycasts at other players and objects.
	private string[] layers;
	//TODO: We may need to tune SCAN_INCR (The amount the scan angle changes on each iteration) for it to feel right
	private const float SCAN_INCR = 15.0f;
	//TODO: This may potentially need to scale with the camera.
	private const float SCAN_DIST = 12.0f;
	
	//Used to in the animator  to know which weapon should be displayed at
	//any given time.
	private bool GrappleEquipped = false;
	private bool SwordEquipped = true;
	
	//Weapon references used to handle switching/use
	public GameObject myGrapple;
	private GameObject myShield;
	private GameObject mySword;
	public GameObject myTorch;
	
	public int playerNum;
	
	//Variables for setting up the controllers through XInput.
	private bool playerIndexSet;
	private GamePadState state;
	private GamePadState prevState;
	
	private Color playerColor;
	public Color PlayerColor {
		get { return playerColor; }
		set { playerColor = value; }
	}

	public AudioClip equipSword;
	public AudioClip equipShield;
	public AudioClip equipTorch;
	public AudioClip equipGrapple;

	private GameObject cameraRef;

	void Start () {
		anim = GetComponent<Animator>();
		playerMovement = new PlayerMovement( anim, rigidbody2D, transform );
		cameraRef = GameObject.FindGameObjectWithTag("MainCamera");
		string temp = "Enemy,Pole,Player1,Player2,Player3,Player4,Trees";
		temp = temp.Replace((tag), "Default");
		layers = temp.Split (',');
		playerTarget = new PlayerTargeting(transform, layers);
		state = GamePad.GetState((PlayerIndex)playerNum);
		SetupItems();
	}

	/*
     * Initialization method for weapons and utility items. Instantiate prefabs, set sizes,
     * and set default active weapons on the player sprite.
	 */
	private void SetupItems() {
		
		Shield.SetActive (true);
		myShield = Instantiate( Shield, Shield.transform.position, Shield.transform.rotation) as GameObject;
		myShield.transform.parent = LeftHand.gameObject.transform;
		myShield.tag = "Shield";
		Vector3 shieldScale = myShield.transform.localScale;
		shieldScale.x = 1.5f;
		shieldScale.y = 1.5f;
		myShield.transform.localScale = shieldScale;
		mySword = Instantiate( PlayerSword, PlayerSword.transform.position, PlayerSword.transform.rotation) as GameObject;
		mySword.transform.parent = RightHand.gameObject.transform;
		//mySword.tag = "Sword";
		Vector3 swordScale = mySword.transform.localScale;
		swordScale.x = 1f;
		swordScale.y = 1f;
		mySword.transform.localScale = swordScale;
		mySword.SetActive (true);

	}

	public void SetInput( InputController inputController ) {
		// input = inputController;
	}

	void Update () {

		
		if (GrappleEquipped && myGrapple == null && currGrapple == null && anim.GetBool("ThrowingGrapple") == false)
		{
			makeGrapple ();
		}

	
//		if (!playerIndexSet || !prevState.IsConnected)
//		{
//			for (int i = 0; i < 4; ++i)
//			{
//				PlayerIndex testPlayerIndex = (PlayerIndex)i;
//				GamePadState testState = GamePad.GetState(testPlayerIndex);
//				if (testState.IsConnected)
//				{
//					Debug.Log(string.Format("GamePad found {0}", testPlayerIndex));
//					playerIndex = testPlayerIndex;
//					playerIndexSet = true;
//					playerNum = 
//				}
//			}
//		}

		if (state.Buttons.X == ButtonState.Pressed){
			//GamePad.SetVibration((PlayerIndex)playerNum, 0.75f, 0.75f);
		}
		else
		{
			//GamePad.SetVibration((PlayerIndex)playerNum, 0f, 0f);
		}
		
		
		prevState = state;
		state = GamePad.GetState((PlayerIndex)playerNum);
		
		// See if player wants to swap left hand (grapple or shield)
		if ( prevState.Buttons.LeftShoulder == ButtonState.Released && state.Buttons.LeftShoulder == ButtonState.Pressed ) {
			if (GrappleEquipped) {
				GrappleEquipped = false;
				Shield.SetActive(true);
				myShield = Instantiate( Shield, Shield.transform.position, Shield.transform.rotation) as GameObject;
				myShield.transform.parent = LeftHand.gameObject.transform;
				Vector3 shieldScale = myShield.transform.localScale;
				shieldScale.x = 1.5f;
				shieldScale.y = 1.5f;
				myShield.transform.localScale = shieldScale;
				Destroy (myGrapple);
				audio.clip = equipShield;
				audio.Play();
			}
			else {
				GrappleEquipped = true;
				GrappleAndRope.SetActive (true);
				myGrapple = Instantiate( GrappleAndRope, GrappleAndRope.transform.position, GrappleAndRope.transform.rotation) as GameObject;
				myGrapple.transform.parent = LeftHand.gameObject.transform;
				Vector3 grappleScale = myGrapple.transform.localScale;
				grappleScale.x = 1.5f;
				grappleScale.y = 1.5f;
				myGrapple.transform.localScale = grappleScale;
				Destroy(myShield);
				audio.clip = equipGrapple;
				audio.Play();
			}
		}

		// See if player wants to swap right hand (sword or torch)
		if ( prevState.Buttons.RightShoulder == ButtonState.Released && state.Buttons.RightShoulder == ButtonState.Pressed ) {
			if (SwordEquipped) {
				SwordEquipped = false;
				Torch.SetActive(true);
				myTorch = Instantiate( Torch, Torch.transform.position, Torch.transform.rotation) as GameObject;
				myTorch.transform.parent = RightHand.gameObject.transform;
				Vector3 torchScale = mySword.transform.localScale;
				torchScale.x = 1f;
				torchScale.y = 1f;
				myTorch.transform.localScale = torchScale;
				Destroy (mySword);
				audio.clip = equipTorch;
				audio.Play();
			}
			else {
				SwordEquipped = true;
				mySword = Instantiate( PlayerSword, PlayerSword.transform.position, PlayerSword.transform.rotation) as GameObject;
				mySword.transform.parent = RightHand.gameObject.transform;
				Vector3 swordScale = mySword.transform.localScale;
				swordScale.x = 1f;
				swordScale.y = 1f;
				mySword.transform.localScale = swordScale;
				mySword.SetActive (true);
				Destroy(myTorch);
				audio.clip = equipSword;
				audio.Play();
			}
		}

		// Check if player is grappling
		if ( state.Triggers.Left  > ROT_DEAD_ZONE && myGrapple != null) {
			if (RtTimer == 0)
			{
				FireOrDestroyGrapple();
			}
			RtTimer += Time.deltaTime;
		}
		else if ( state.Triggers.Left  <= ROT_DEAD_ZONE ) {
			RtTimer = 0f;
		}

		// Check if player is using shield
		if ( state.Triggers.Left  > ROT_DEAD_ZONE && GrappleAndRope.activeSelf == false && anim.GetBool ("Blocking") == false && myShield != null && anim.GetBool("StopBlock") == false) {
			// enable shield collider
			myShield.GetComponent<BoxCollider2D>().enabled = true;
			anim.SetBool("Blocking", true);

		}
		else if ( state.Triggers.Left  <= ROT_DEAD_ZONE && anim.GetBool ("Blocking") == true && anim.GetBool ("StopBlock") == false) {
			anim.SetBool ("StopBlock",true);
			Invoke ("DoneBlock", .16f);		
		}
		else if (myShield == null){ 
			anim.SetBool ("Blocking",false);
		}

		// Check for attack
		if( state.Triggers.Right > ROT_DEAD_ZONE && anim.GetBool("Attack") == false && mySword != null) {
			Invoke ("SwordColliderOn", .3f);
			anim.SetBool ("WaveTorch", false);
			anim.SetBool( "Attack", true );
			
		}

		// Check for torch wave
		if( state.Triggers.Right > ROT_DEAD_ZONE && anim.GetBool("WaveTorch") == false && myTorch != null) {
			// Trigger sword swipe 
			anim.SetBool ("Attack", false);
			myTorch.GetComponent<CircleCollider2D>().enabled = true;
			anim.SetBool( "WaveTorch", true );
		}
		else if (state.Triggers.Right <= ROT_DEAD_ZONE && myTorch != null) {
			myTorch.GetComponent<CircleCollider2D>().enabled = false;
			if( anim.GetBool( "WaveTorch" ) ) {
				ClearTorchParticles();
			}
			anim.SetBool("WaveTorch",false);
		}

		// if player pushs their anchor button, set them anchored
		if( state.Buttons.A == ButtonState.Pressed ){
			playerAnchored = true;
		}
		else{
			playerAnchored = false;
		}

		if( state.Buttons.Y == ButtonState.Pressed && prevState.Buttons.Y == ButtonState.Released){
			cameraRef.GetComponent<CameraController>().pauseGame();
		}

	}

	// If a grapple doesn't exist, fire one, else delete the current one
	private void FireOrDestroyGrapple() {
		if( grappleExists() ) {
			anim.SetBool ("ThrowingGrapple", false);
			Destroy( currGrapple );
			if (myGrapple == null)
				makeGrapple ();

		}
		else {
			FireGrapple();
			if(anim.GetBool ("ThrowingGrapple") == false)
			{
				anim.SetBool("ThrowingGrapple",true);
				Invoke ("DestroyGrapple",.1f);
				Invoke ("stopThrowingGrapple",.8f);
			}

		}
	}

	private bool grappleExists() { 
		return currGrapple != null;
	}

	private void FireGrapple() { 
		// Set new grapple position to be slightly in front of the player
		GameObject newGrapple = Instantiate( Grapple, GrappleSpawn.position, GrappleSpawn.rotation) as GameObject;
		float xv;
		float yv;
		if (playerTarget.getTS().lockedOn){
		  	AFacesB(newGrapple, playerTarget.getTS().lockedTo);
			xv = ( newGrapple.transform.up * GrappleSpeed ).x;
			yv = ( newGrapple.transform.up * GrappleSpeed ).y;
		}
		else {
			newGrapple.transform.rotation = Quaternion.Euler (0, 0, transform.eulerAngles.z-180);
			xv = ( newGrapple.transform.up * GrappleSpeed ).x;
			yv = ( newGrapple.transform.up * GrappleSpeed ).y;
		}
		newGrapple.rigidbody2D.velocity = new Vector2( xv, yv );
		newGrapple.GetComponent<Grapple>().setWhoFired(gameObject);
		currGrapple = newGrapple;
		Vector3 grapScale = currGrapple.transform.localScale;
		grapScale.x = 0.8f;
		grapScale.y = 0.8f;
		currGrapple.transform.localScale = grapScale;
	}

	void FixedUpdate() {
		SetMovement();
		if ( state.Buttons.LeftStick == ButtonState.Pressed ){	
			playerTarget.CheckTarget();
			playerTarget.Targeting(state.ThumbSticks.Right.X);
		}
		else{
			playerTarget.clearTarState ();
			SetRotation();
		}
		MoveToGrapple();
	}

	// If the grapple exists, and it's attached to something, move towards it
	private void MoveToGrapple(){
		if(grappleExists()){
			if(currGrapple.GetComponent<Grapple>().isAttached()){
				if(!playerAnchored){
					transform.position = Vector3.MoveTowards(transform.position, currGrapple.transform.position, moveToGrappleSpeed);
				}
			}
		}
	}
	
	// Gets movement from left axis and applies it
	private void SetMovement() {
		float currRotation = transform.eulerAngles.z;
		playerMovement.SetMovement( state.ThumbSticks.Left.X ,
		-state.ThumbSticks.Left.Y, PlayerSpeed, currRotation );
	}

	// Gets rotation from right axis, applies it. You can't spin 180 degrees instantly however 
	private void SetRotation() {
		float rotX = -state.ThumbSticks.Right.X;
		float rotY = state.ThumbSticks.Right.Y;
		float desiredAngle = Mathf.Atan2( rotX, rotY ) * Mathf.Rad2Deg + 180f;
		float currAngle = transform.eulerAngles.z;

		/* Get the next angle we should be facing.  This is neccessary so that we can move
		   player to the desired angle over several frame to make the animation smoother */
		float nextAngle = GetNextAngle( desiredAngle, currAngle );

		if( Mathf.Abs( rotX ) > ROT_DEAD_ZONE || Mathf.Abs( rotY ) > ROT_DEAD_ZONE ) {
			transform.rotation = Quaternion.Euler(0, 0, nextAngle);
			rigidbody2D.angularVelocity = 0;
		}
	}

	private float GetNextAngle( float desiredAngle, float currAngle ) {
		/* Get the shortest angle between the current angle we are facing and the angle
		   we desire to be facing.  The sign of the angle between tells us which is the
		   shortest way to rotate to get to the desired angle */
		float angleBetween = Mathf.Atan2( Mathf.Sin( ( desiredAngle - currAngle ) * Mathf.Deg2Rad ),
		        Mathf.Cos( ( desiredAngle - currAngle ) * Mathf.Deg2Rad ) ) * Mathf.Rad2Deg;
		float nextAngle = currAngle;

		if( Mathf.Abs( angleBetween ) < RotSpeed ) {
			nextAngle = desiredAngle;
		}
		else if( angleBetween < 0 ) {
			nextAngle -= RotSpeed;
		}
		else {
			nextAngle += RotSpeed;
		}

		return nextAngle;
	}

	private void DoneAttack() {
		if (mySword != null)
		{
			mySword.GetComponent<BoxCollider2D>().enabled = false;
		}
		anim.SetBool( "Attack", false );
	}

	private void DoneBlock() {
		if (myShield != null)
		{
			myShield.GetComponent<BoxCollider2D>().enabled = false;
		}
		anim.SetBool ("StopBlock",false);
		anim.SetBool ("Blocking",false);

	}

	
	public void DestroyGrapple() {
		//if (myGrapple != null)
		//{
		Destroy(myGrapple);
		//}
	}
	
	public void makeGrapple() {
		if (myShield == null) {
						GrappleEquipped = true;
						myGrapple = Instantiate (GrappleAndRope, GrappleAndRope.transform.position, GrappleAndRope.transform.rotation) as GameObject;
						myGrapple.transform.parent = LeftHand.gameObject.transform;
						Vector3 grappleScale = myGrapple.transform.localScale;
						grappleScale.x = 1.5f;
						grappleScale.y = 1.5f;
						myGrapple.transform.localScale = grappleScale;
						myGrapple.SetActive (true);
				}
	}
	
	public void stopThrowingGrapple()
	{
		anim.SetBool ("ThrowingGrapple", false);
	}

	
	//helper method used to set the player facing whatever object it is locked to.
	private void AFacesB(GameObject A, GameObject B) {
		/* Get rotation we need to be set to to be facing the player.  We can do this with
		   ours and the players transforms */
		float rotation = Mathf.Atan2 (B.transform.position.y - A.transform.position.y,
		                              B.transform.position.x - A.transform.position.x) * Mathf.Rad2Deg - 90;
		A.transform.eulerAngles = new Vector3 (0, 0, rotation);
		
	}

	/**
	 * DEATHSCRIPT
	 *	This Script runs when the knight dies. It sets the knights items to hav null parents so they do not move.
	 */
	 private void knightDeath()
	 {
	 	foreach (Transform child in transform)
	 	{
	 		if (child.tag == "Sword")
	 		{
	 			child.parent = null;
	 		}
	 		else if (child.tag == "Torch")
	 		{
	 			child.parent = null;
	 		}
	 		else if (child.tag == "Shield")
	 		{
	 			child.parent = null;
	 		}
	 		else if (child.tag == "Grapple")
	 		{
	 			child.parent = null;
	 		}
	 		else
	 		{
	 			// Do nothing, the child is not an item
	 		}
	 	}
	 }


	private void SwordColliderOn() {
		if (mySword != null)
			mySword.GetComponent<BoxCollider2D>().enabled = true;

	}

	public void activateColour(int playerNum){
		Transform helm, right, left;
		switch(playerNum){
			case 1:
				PlayerColor = new Color( 160f / 255f, 0, 0 );
				right = transform.Find("KnightRightPauldron/RedPauldron");
				right.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				left = transform.Find("KnightLeftPauldron/RedPauldron");
				left.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				helm = transform.Find("KnightHelm/RedHelm");
				helm.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				break;
			case 2:
				PlayerColor = new Color( 0, 170f / 255f, 0 );
				right = transform.Find("KnightRightPauldron/GreenPauldron");
				right.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				left = transform.Find("KnightLeftPauldron/GreenPauldron");
				left.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				helm = transform.Find("KnightHelm/GreenHelm");
				helm.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				break;
			case 3:
				PlayerColor = Color.blue;
				right = transform.Find("KnightRightPauldron/BluePauldron");
				right.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				left = transform.Find("KnightLeftPauldron/BluePauldron");
				left.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				helm = transform.Find("KnightHelm/BlueHelm");
				helm.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				break;
			case 4:
				PlayerColor = Color.yellow;
				right = transform.Find("KnightRightPauldron/YellowPauldron");
				right.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				left = transform.Find("KnightLeftPauldron/YellowPauldron");
				left.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				helm = transform.Find("KnightHelm/YellowHelm");
				helm.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				break;
		}
	}

	// Delete particles leftover during torch wave so they don't reappear when you
	// wave again later
	public void ClearTorchParticles() {
		GetComponentInChildren<FireParticles>().DestroyAllParticles();
	}

	public void KillPlayer() {
		Destroy( gameObject );
	}

	void OnDestroy() {
		if( currGrapple != null ) {
			Destroy( currGrapple );
		}
	}
}