using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerHealthManager : MonoBehaviour {
	private const int LEFT = 200;
	private const int NAME_TOP = 10;
	private const int BAR_TOP = 40;
	private const int WIDTH = 200;
	private const int NAME_HEIGHT = 30;
	private const int BAR_HEIGHT = 10;
	private const int BUFFER = 50;

	public PlayerManager PlayerManager;

	// Update is called once per frame
	void OnGUI() {
		List<GameObject> players = PlayerManager.GetPlayers();
		int i = 0;
		foreach( GameObject player in players ) {
			DisplayPlayerHealth( player.GetComponent<Player>().PlayerColor, player.GetComponentInChildren<PlayerBody>(), i, player.tag );
			i++;
		}
	}

	private void DisplayPlayerHealth( Color playerColor, PlayerBody player, int position, string name ) {
		int health = player.GetHealth();
		int maxHealth = player.GetMaxHealth();
		DisplayName( playerColor, name, position );
		DisplayHealthBar( health, maxHealth, position );
	}
	
	private void DisplayName( Color color, string name, int position ) {
		GUIStyle style = new GUIStyle();
		style.normal.textColor = color;
		style.fontSize = 20;
		style.fontStyle = FontStyle.Bold;
		GUI.Label(new Rect( position * LEFT + BUFFER, NAME_TOP, WIDTH, NAME_HEIGHT ), name, style);
	}
	
	private void DisplayHealthBar( int health, int maxHealth, int position ) {
		//Draw Background Rect
		Texture2D red = new Texture2D( 1, 1 );
		red.SetPixel( 1, 1, Color.red );
		red.Apply();
		GUI.DrawTexture( new Rect( position * LEFT + BUFFER, BAR_TOP, maxHealth, BAR_HEIGHT ), red );
		
		//Draw Foreground Rect
		Texture2D green = new Texture2D( 1, 1 ); //Creates 2D texture
		green.SetPixel( 1, 1, Color.green ); //Sets the 1 pixel to be green
		green.Apply(); //Applies all the changes made
		GUI.DrawTexture( new Rect( position * LEFT + BUFFER, BAR_TOP, health, BAR_HEIGHT ), green );
	}
}

