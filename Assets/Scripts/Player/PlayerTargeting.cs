using UnityEngine;
using System;

public class PlayerTargeting
{

	//Cooldown for locking on to a new target.
	private const float LOCKON_COOLDOWN = 0.25f;
	//Joystick rotation dead zone.
	private const float ROT_DEAD_ZONE = 0.2f;
	//TODO: We may need to tune SCAN_INCR (The amount the scan angle changes on each iteration) for it to feel right
	private const float SCAN_INCR = 5.0f;
	//TODO: This may potentially need to scale with the camera.
	private const float SCAN_DIST = 12.0f;
	private string[] layers;
	private TargetState ts;
	private Transform transform;
	
	
	public PlayerTargeting (Transform t, string[] s)
	{
		layers = s;
		transform = t;
		ts.lastLocked = null;
		ts.lockedOn = false;
		ts.lockedTo = null;
		ts.onCD = false;
		ts.scanAngle = transform.rotation.eulerAngles.z;
		ts.scanDirection = 180.0f;
	}
	
	public void CheckTarget(){
		if (ts.lockedOn && ts.lockedTo == null){
			ts.lockedOn = false;
		}
	}
	
	public TargetState getTS(){
		return ts;
	}
	
	//used to clear Targeting method information once the Targeting button is released.
	//the targetstate will always maintain it's coldown information.
	public void clearTarState(){
		ts.lockedOn = false;
		ts.lockedTo = null;	
		ts.lastLocked = null;
		ts.scanAngle = transform.eulerAngles.z;
		ts.scanDirection = 0.0f;
	}
	
	public void Targeting(float rotX){
		bool switchTar = false;
		
		
		//checking to see if we have a direction to scan in
		if (Mathf.Abs (rotX) > ROT_DEAD_ZONE){
			switchTar = true;
			ts.scanDirection = -Mathf.Sign(rotX);
		}
		else{
			//not locked onto something and no direction is specified
			//wait for an update where player specifies a target direction
			if (!ts.lockedOn) return;
		}
		
		//Seeing if the targetting system is off cooldown yet
		if (ts.onCD && (ts.lastLockedTime + LOCKON_COOLDOWN < Time.time)){
			ts.onCD = false;
		}
		
		//If we are locked onto an object and have no direction to search in
		// or our targetting system is on cooldown, just continue to face the object
		if (ts.lockedOn && (!switchTar || ts.onCD))
		{
			SetFacingObject (ts.lockedTo);
			return;
		}
		
		//If we aren't locked on to anything then we know we can scan without worrying about
		//finding the same object again
		if (!ts.lockedOn){
			
			Scan();
			if (ts.lockedOn){
				//if we found something, face it
				SetFacingObject (ts.lockedTo);
			}
			return;
		}
		
		//If we're locked onto something and looking for a new object,
		//we need to make sure we don't find the ccurrent object again (since it should be at the start of the scan.
		if (ts.lockedOn && !ts.onCD){
			ScanSetup ();
			Scan ();
			if (ts.lockedOn){
				//if we found something, face it
				SetFacingObject (ts.lockedTo);
			}
			return;
		}
		
	}
	
	//set up the Targeting state for locking onto a new target
	private void ScanSetup(){
		//lastlocked needs to be ignored on the next scan
		ts.lastLocked = ts.lockedTo;
		//set lockedTo pointer to null
		ts.lockedTo = null;
		//locked on is false
		ts.lockedOn = false;
		//scan angle needs to be straight ahead from where the player is facing.
		ts.scanAngle = transform.eulerAngles.z;
	}
	
	//Perform a 360 degree scan looking for game objects to lock on to.
	private void Scan(){
		
		RaycastHit2D r;
		GameObject found;
		float start = ts.scanAngle;
		
		
		
		while (!ts.lockedOn && (Mathf.Abs(ts.scanAngle-start) < 360.0f)){
			
			r = Physics2D.Raycast(transform.position, PlayerTargeting.angleToVec2(ts.scanAngle+180), SCAN_DIST, LayerMask.GetMask (layers));
			
			if (r.collider != null){
				found = r.transform.gameObject;
				
				if ((ts.lastLocked == null || found != ts.lastLocked)){
					ts.lockedOn = true;
					ts.lockedTo = found;
					ts.onCD = true;
					ts.lastLockedTime = Time.time;
					ts.lastLocked = null;
				}
			}
			
			ts.scanAngle = (ts.scanAngle + ts.scanDirection*SCAN_INCR);
		}
	}
	
	//helper method to convert an angle in degrees into a vector2 for use in a raycast.
	//may seem a bit confusing at first, because degrees are always measured starting from
	//90.0. This is because the player transform's rotation is 0 when it is facing up.
	static public Vector2 angleToVec2(float ang){
		Vector2 v = Vector2.up;
		
		float sin = Mathf.Sin(ang * Mathf.Deg2Rad);
		float cos = Mathf.Cos(ang * Mathf.Deg2Rad);
		
		float tx = v.x;
		float ty = v.y;
		v.x = (cos * tx) - (sin * ty);
		v.y = (sin * tx) + (cos * ty);
		
		return v;
	}
	
	//helper method used to set the player facing whatever object it is locked to.
	private void SetFacingObject(GameObject obj) {
		/* Get rotation we need to be set to to be facing the player.  We can do this with
		   ours and the players transforms */
		float rotation = Mathf.Atan2 (obj.transform.position.y - transform.position.y,
		                              obj.transform.position.x - transform.position.x) * Mathf.Rad2Deg + 90;
		transform.eulerAngles = new Vector3 (0, 0, rotation);
		
	}
		
	//structure used to manage the state of the targetting mechanism.
	public struct TargetState
	{
		//scan angle of 0.0f is equal to Vec2.up when passed into angToVec2
		public float scanAngle;
		public float scanDirection;
		public GameObject lastLocked;
		public GameObject lockedTo;
		public bool lockedOn;
		public bool onCD;
		public float lastLockedTime;
	}
}


