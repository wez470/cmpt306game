﻿using UnityEngine;
using System.Collections;

public class PlayerBody : MonoBehaviour {
	private Health playerHealth = new Health( 150 );
	public AudioClip playerHit1;
	public AudioClip playerHit2;
	public AudioClip playerHit3;

	float rand = 0.0f;
	public void DecreaseHealth( int amount ) {
		rand = Random.Range(0.0f,10.0f);
		if(rand >= 6.66f){
			audio.clip = playerHit1;
		}
		else if(rand >= 3.33f){
			audio.clip = playerHit2;
		}
		else{
			audio.clip = playerHit3;
		}
		audio.Play();
		playerHealth.DecreaseHealth( amount );
		if( playerHealth.GetHealth() <= 0 ) {
			GameObject playerManager = GameObject.FindGameObjectWithTag( "PlayerManager" );
			playerManager.GetComponent<PlayerManager>().KillPlayer( transform.parent.gameObject );
			GetComponentInParent<Player>().KillPlayer();
		}
	}
	
	public int GetHealth() {
		return playerHealth.GetHealth();
	}
	
	public int GetMaxHealth() {
		return playerHealth.GetMaxHealth();
	}
}
