﻿public class PlayerPS4Controller : InputController {
	private int playerNum;

	public PlayerPS4Controller( int playerNumber )
	{
		this.playerNum = playerNumber;
	}

	string InputController.getGrappleButton() 
	{
		return "joystick button 6";
	}

	string InputController.getSwitchLeftHand() 
	{
		return "joystick button 4";
	}

	string InputController.getSwitchRightHand() 
	{
		return "joystick button 5";
	}
	
	string InputController.getAttackButton() 
	{
		return "joystick button 7";
	}
	
	string InputController.getAnchorButton() 
	{
		return "joystick button 1";
	}
	
	string InputController.getTargetingButton() 
	{
		return "joystick button 10";
	}
	
	string InputController.getXAxisMovement() 
	{
		return "X-Axis";
	}
	
	string InputController.getYAxisMovement() 
	{
		return "Y-Axis";
	}
	
	string InputController.getXAxisRotation() 
	{
		return "3rd Axis";
	}
	
	string InputController.getYAxisRotation() 
	{
		return "4th Axis";
	}
}