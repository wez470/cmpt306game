using UnityEngine;

public class PlayerMovement {
	private const int MAX_X_DIST_FROM_CAMERA = 33;
	private const int MAX_Y_DIST_FROM_CAMERA = 9;
	private const int CLOSE_ANGLE_DIST = 55;
	private const int FAR_ANGLE_DIST = 125;
	private const int DIST_BETWEEN_STRAFE_ANGLES = FAR_ANGLE_DIST - CLOSE_ANGLE_DIST;

	private Animator playerAnim;
	private Rigidbody2D rigidbody2D;
	private Transform transform;
	private float playerSpeed;

	public PlayerMovement( Animator playerAnim, Rigidbody2D rigidbody2D, Transform transform ) {
		this.playerAnim = playerAnim;
		this.rigidbody2D = rigidbody2D;
		this.transform = transform;
	}
	
	// Gets movement from left axis and applies it
	public void SetMovement( float inputX, float inputY, float speed, float rotation ) {
		this.playerSpeed = speed;
		float speedX = inputX * playerSpeed;
		float speedY = -inputY * playerSpeed;

		SetStrafing( speedX, speedY, rotation );

		ClampAndSetMovement( speedX, speedY );
		playerAnim.SetFloat( "Speed", rigidbody2D.velocity.magnitude ); 
	}

	private void SetStrafing( float speedX, float speedY, float rotation ) {
		// Get the angle of the current direction we are moving
		Quaternion movementDirection = Quaternion.Euler( new Vector3( 0, 0, Mathf.Atan2( speedY, speedX ) * Mathf.Rad2Deg ) );
		float movementRot = movementDirection.eulerAngles.z;
		
		// Get strafing zones
		float closeLowerStrafe = Quaternion.Euler( new Vector3( 0, 0, movementRot - CLOSE_ANGLE_DIST ) ).eulerAngles.z;
		float farLowerStrafe = Quaternion.Euler( new Vector3( 0, 0, movementRot - FAR_ANGLE_DIST ) ).eulerAngles.z;
		float closeHigherStrafe = Quaternion.Euler( new Vector3( 0, 0, movementRot + CLOSE_ANGLE_DIST ) ).eulerAngles.z;
		float farHigherStrafe = Quaternion.Euler( new Vector3( 0, 0, movementRot + FAR_ANGLE_DIST ) ).eulerAngles.z;
		
		// Adjust angle player is looking to match the movement angle orientation ( i.e. the angles considered 0 for
		// movement and rotation start at the same place )
		float adjustedRotation = Quaternion.Euler( new Vector3( 0, 0, rotation - 90 ) ).eulerAngles.z;
		
		// check if the direction we are looking means we should be strafing left
		if( Mathf.Abs( Mathf.DeltaAngle( adjustedRotation, closeLowerStrafe ) ) < DIST_BETWEEN_STRAFE_ANGLES
		        && Mathf.Abs( Mathf.DeltaAngle( adjustedRotation, farLowerStrafe ) ) < DIST_BETWEEN_STRAFE_ANGLES ) {
			playerAnim.SetBool( "StrafeRight", false );
			playerAnim.SetBool( "StrafeLeft", true );
			
		}
		// check if the direction we are looking means we should be strafing right
		else if( Mathf.Abs( Mathf.DeltaAngle( adjustedRotation, closeHigherStrafe ) ) < DIST_BETWEEN_STRAFE_ANGLES
		        && Mathf.Abs( Mathf.DeltaAngle( adjustedRotation, farHigherStrafe ) ) < DIST_BETWEEN_STRAFE_ANGLES ) {
			playerAnim.SetBool( "StrafeRight", true );
			playerAnim.SetBool( "StrafeLeft", false );
		}
		// no strafing happening
		else {
			playerAnim.SetBool( "StrafeRight", false );
			playerAnim.SetBool( "StrafeLeft", false );
		}
	}

	private void ClampAndSetMovement( float speedX, float speedY ) {
		float cameraX = Camera.main.transform.position.x;
		float cameraY = Camera.main.transform.position.y;
		float playerX = transform.position.x;
		float playerY = transform.position.y;
		
		// Clamp movement so player can't move off the screen
		if( playerY >= cameraY + MAX_Y_DIST_FROM_CAMERA ) {
			speedY = Mathf.Clamp( speedY, -playerSpeed, 0 );
		}
		if( playerY <= cameraY - MAX_Y_DIST_FROM_CAMERA ) {
			speedY = Mathf.Clamp( speedY, 0, playerSpeed );
		}
		if( playerX <= cameraX - MAX_X_DIST_FROM_CAMERA ) {
			speedX = Mathf.Clamp( speedX, 0, playerSpeed );		
		}
		if( playerX >= cameraX + MAX_X_DIST_FROM_CAMERA ) {
			speedX = Mathf.Clamp( speedX, -playerSpeed, 0 );
		}
		
		rigidbody2D.AddForce( new Vector2( speedX, speedY ) );
	}
}
