﻿using UnityEngine;
using System;
using System.Collections.Generic;
using XInputDotNetPure;

public class PlayerManager : MonoBehaviour {
	public GameObject Player;
	public bool PS4Controller;	// Variable to determine if PS4 controller being used.

	private int numPlayers = 0;
	private int startingNumPlayers = 0;
	private List<GameObject> players = new List<GameObject>();

	void Awake () {
		foreach( string joystick in Input.GetJoystickNames() ) {
			// make sure device is a controller
			if( joystick.Contains( "Controller" ) ) {
				numPlayers++;
			}
			startingNumPlayers = numPlayers;
		}
		SetupPlayers();
	}

	//Create a player instance foreach controller detected
	private void SetupPlayers() {
		for( int i = 1; i <= numPlayers; i++ ) {
			CreatePlayer( i );
		}
	}

	// Assigns a controller and tag value foreach player
	private void CreatePlayer( int playerNum ) {
		GameObject newPlayer = Instantiate( Player ) as GameObject;
		newPlayer.transform.position = new Vector3( 0, 0, 0 );
		SetupNewPlayerController( playerNum, newPlayer );
		newPlayer.tag = "Player" + playerNum;
		newPlayer.layer = LayerMask.NameToLayer("Player" + playerNum);
		newPlayer.GetComponentInChildren<PlayerBody>().gameObject.layer = LayerMask.NameToLayer ("Player" + playerNum);
		newPlayer.GetComponentInChildren<Player>().activateColour(playerNum);
		newPlayer.GetComponentInChildren<Player>().playerNum = playerNum-1;
		players.Add( newPlayer );
	}

	private void SetupNewPlayerController( int playerNum, GameObject player ) 
	{
		if (!PS4Controller)
		{
			player.GetComponent<Player>().SetInput( new PlayerXboxController( playerNum ) );
		}
		else
		{
			player.GetComponent<Player>().SetInput( new PlayerPS4Controller( playerNum ) );
		}	
	}

	public int GetNumPlayers() {
		return players.Count;
	}

	public List<GameObject> GetPlayers() {
		return players;
	}

	// returns the closest player to a given point or null of no players exist
	public GameObject GetClosestPlayer( Vector3 position ) {
		float closestDist = float.MaxValue;
		GameObject closestPlayer = null;

		foreach( GameObject player in players ) {
			float currDist = Vector3.Distance( player.transform.position, position );

			if( currDist < closestDist ) {
				closestDist = currDist;
				closestPlayer = player;
			}
		}

		return closestPlayer;
	}

	public void KillPlayer( GameObject player ) {
		players.Remove( player );
		if( players.Count <= 0 ) {
			//make sure rumble is off on all controllers.
			for (int i = 0; i < startingNumPlayers; i++){
				GamePad.SetVibration((PlayerIndex)i, 0, 0);
			}
			Application.LoadLevel( "MainMenu" );
		}
	}
}
