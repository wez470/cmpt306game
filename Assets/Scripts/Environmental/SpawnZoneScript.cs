﻿using UnityEngine;
using System.Collections;

public class SpawnZoneScript : MonoBehaviour 
{
	// Variables!
	private int playerCount;

	// Use this for initialization
	void Start () 
	{
		// Declare initial variables
		playerCount = 0;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	// This method returns the playerCount
	public int getPlayerCount()
	{
		return playerCount;
	}

	// This method incriments the player count for this region.
	void OnTriggerEnter2D(Collider2D col)
	{
		if (col.tag == "PlayerChest")
		{
			playerCount += 1;
			//Debug.Log("Player entered zone: " + playerCount);
		}
	}
	// This method decriments the player count for this region.
	void OnTriggerExit2D(Collider2D col)
	{
		if (col.tag == "PlayerChest")
		{
			playerCount -= 1;
			//Debug.Log("Player exited zone: " + playerCount);
		}
	}
}
