﻿using UnityEngine;
using System.Collections;

public class EnemySpawnController : MonoBehaviour 
{
	// VARIABLES
	// Spawn Variables
	public GameObject goblinPrefab;
	public GameObject ogrePrefab;

	// Spawn Points
	public GameObject NWSpawnPoint;
	public GameObject NESpawnPoint;
	public GameObject SWSpawnPoint;
	public GameObject SESpawnPoint;
	private bool NWSpawnActive;
	private bool NESpawnActive;
	private bool SWSpawnActive;
	private bool SESpawnActive;
	private int lowestCount;

	// Spawn Checking variables
	private float lastCheckTime;
	public float spawnTimer = 5.0f;
	private GameObject[] goblins;
	private GameObject[] ogres;

	// Spawn Rate Variables
	public int goblinHorde = 4;
	public int ogreHorde = 1;
	public int waveMultiplier = 5;
	public int spawnThreshold = 1;
	private int livingGoblins = 0;
	private int livingOgres = 0;
	private int temp;
	private bool spawnGoblins;

	public AudioClip spawnNoise;

	private int wave;

	public int Wave { get { return wave; }	}

	// Use this for initialization
	void Start () 
	{
		audio.clip = spawnNoise;
		NWSpawnActive = false;
		NESpawnActive = false;
		SWSpawnActive = false;
		SESpawnActive = false;
		lastCheckTime = Time.time;
		livingGoblins = 0;
		livingOgres = 0;
		temp = 0;
		spawnGoblins = true;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if ((Time.time-spawnTimer) < lastCheckTime)
		{
			livingGoblins = GameObject.FindGameObjectsWithTag("Goblin").Length;
			livingOgres = GameObject.FindGameObjectsWithTag("Ogre").Length;


			if (livingOgres < 1 && livingGoblins < 1 )
			{
				if(wave != 0){
					audio.Play();
				}
				wave++;
				spawnGoblins = true;
				checkSpawns();
				spawnOgres();
			}

			lastCheckTime = Time.time;
		}
		
	}


	// Method checks the spawn points and activates ones for spawning.
	void checkSpawns ()
	{
		lowestCount = 5; // set to 5 because there are only ever 4 players, so it will get updated.
		NWSpawnActive = false;
		NESpawnActive = false;
		SWSpawnActive = false;
		SESpawnActive = false;
		// First, determines what the lowest players in any zone are.
		foreach (SpawnZoneScript childScript in GetComponentsInChildren<SpawnZoneScript>())
		{
    		if (childScript.getPlayerCount() < lowestCount)
    		{
    			lowestCount = childScript.getPlayerCount();
    		}
		}
		// Second, set zones that match the lowest player count to active spawn zones
		foreach (Transform child in this.transform)
		{
			if ((child.name == "NWquarter") && (child.GetComponent<SpawnZoneScript>().getPlayerCount() == lowestCount))
			{
				//Debug.Log("NW Quarter Active");
				NWSpawnActive = true;
			}
			else if ((child.name == "NEquarter") && (child.GetComponent<SpawnZoneScript>().getPlayerCount() == lowestCount))
			{
				//Debug.Log("NE Quarter Active");
				NESpawnActive = true;
			}
			else if ((child.name == "SWquarter") && (child.GetComponent<SpawnZoneScript>().getPlayerCount() == lowestCount))
			{
				//Debug.Log("SW Quarter Active");
				SWSpawnActive = true;
			}
			else if ((child.name == "SEquarter") && (child.GetComponent<SpawnZoneScript>().getPlayerCount() == lowestCount))
			{
				//Debug.Log("SE Quarter Active");
				SESpawnActive = true;
			}
			else
			{
				// do nothing, this spawn site wont be active.
			}
		}

		

		// Thirdly, cycle through active spawns until there are no more spawns.
		while (spawnGoblins)
		{
			// Spawn a mob in the NW quarter
			if (NWSpawnActive && (temp < goblinHorde))
			{
				// Instantiate a goblin here
				//Debug.Log("Spawned Goblin... NW");
				Instantiate(goblinPrefab, NWSpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}
			// Spawn a mob in the NE quarter
			if (NESpawnActive && (temp < goblinHorde))
			{
				// Instantiate a goblin here
				//Debug.Log("Spawned Goblin... NE");
				Instantiate(goblinPrefab, NESpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}
			// Spawn a mob in the SW quarter
			if (SWSpawnActive && (temp < goblinHorde))
			{
				// Instantiate a goblin here
				//Debug.Log("Spawned Goblin... SW");
				Instantiate(goblinPrefab, SWSpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}
			// Spawn a mob in the SE quarter
			if (SESpawnActive && (temp < goblinHorde))
			{
				// Instantiate a goblin here
				//Debug.Log("Spawned Goblin... SE");
				Instantiate(goblinPrefab, SESpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}

			// Check to see if there are any more spawns available, prepare horde size for next wave
			if (temp >= goblinHorde)
			{
				spawnGoblins = false;
				temp = 0;
				goblinHorde = goblinHorde + waveMultiplier;
			}

		}
	}

	// This method spawns ogres
	void spawnOgres()
	{
		temp = 0;
		for (int i = 0; i < ogreHorde; i++)
		{
			if (NESpawnActive && (temp < ogreHorde))
			{
				Instantiate(ogrePrefab, NESpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}
			if (SWSpawnActive && (temp < ogreHorde))
			{
				Instantiate(ogrePrefab, SWSpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}
			if (SESpawnActive && (temp < ogreHorde))
			{
				Instantiate(ogrePrefab, SESpawnPoint.transform.position, Quaternion.identity);
				temp += 1;
			}
		}
		temp = 0;
		ogreHorde = ogreHorde + 1;
	}

}



























