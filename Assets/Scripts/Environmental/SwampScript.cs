﻿// CMPT306, Royal Rescue
// Lealand Adam, 11077800

using UnityEngine;
using System.Collections;

public class SwampScript : MonoBehaviour {

	// Variables!
	public float slowEffect = 0.25f;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}

	/*
	 * SLOWTRIGGER
	 *	When a Player, Goblin, or Ogre enter the branches of a tree, their speed is slowed
	 */
	 void OnTriggerStay2D (Collider2D col)
	 {
	 	// Check to see if the col has is a Player
		if( col.collider2D.tag == "PlayerChest" ) {
			col.collider2D.gameObject.GetComponentInParent<Rigidbody2D>().velocity *= slowEffect;
		}
		// Check to see if the col is an Ogre or Goblin
		if( col.collider2D.tag == "Ogre" || col.collider2D.tag == "Goblin" ) {
			col.collider2D.gameObject.GetComponent<Rigidbody2D>().velocity *= slowEffect;
		}
	 }
}
