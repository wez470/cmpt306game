public interface InputController {
	string getGrappleButton();

	string getSwitchLeftHand();

	string getSwitchRightHand();

	string getAttackButton();
	
	string getAnchorButton();
	
	string getTargetingButton();

	string getXAxisMovement();

	string getYAxisMovement();

	string getXAxisRotation();

	string getYAxisRotation();
}
