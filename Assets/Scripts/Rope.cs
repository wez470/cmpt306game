﻿using UnityEngine;
using System.Collections;

public class Rope : MonoBehaviour {
	public Material rope;
	private Material myRope;
	public float distPlayerToGrapple;
	private Transform playerHand;
	private Transform grapple;

	void Start () {
		// Must make a copy of the material so we are not editing the global material.
		// Editing the global material would mean we would edit every rope instance's texture
		myRope = new Material( rope );
		GetComponent<MeshRenderer>().material = myRope;
		GetComponent<MeshRenderer>().sortingOrder = 3;
	}
	
	void Update () {
		// set our tiling scale so we tile the sprite 
		myRope.mainTextureScale = new Vector2( myRope.mainTextureScale.x, transform.localScale.y );
		setPosition();
		setScale();
		setRotation();
	}

	// set position to be in the middle of the player and grapple
	private void setPosition() {
		transform.position = ( playerHand.position + grapple.position ) / 2;
	}

	// scale texture to dist between player and grapple
	float yScale;
	Vector3 scale;
	private void setScale() {
		distPlayerToGrapple = Vector3.Distance( playerHand.transform.position, grapple.position );
		yScale = distPlayerToGrapple;
		scale = transform.localScale;
		scale.y = yScale;
		transform.localScale = scale;
	}
	
	// rotate to be in line with player and grapple
	Vector3 rotation;
	float rotZ;
	private void setRotation() {
		rotation = playerHand.position - grapple.position;
		rotZ = Mathf.Atan2( rotation.y, rotation.x ) * Mathf.Rad2Deg;
		Quaternion.Euler( new Vector3( 0, 0, rotZ ) );
		transform.rotation = Quaternion.Euler( new Vector3( 0, 0, rotZ + 90 ) );
	}

	public void SetPlayerAndGrapple( Transform playerHand, Transform grapple ) {
		this.playerHand = playerHand;
		this.grapple = grapple;
	}

	void OnDestroy() {
		Destroy( myRope );
	}
}
