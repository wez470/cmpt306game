﻿using UnityEngine;
using System.Collections.Generic;

public class CameraController : MonoBehaviour {
	private const float CAMERA_DEPTH = -60f;
	private const int EDGE_BUFFER = 2;

	public PlayerManager PlayerManager;

	private List<GameObject> players;

	//The extreme positions of all the players combined each update
	private float minX = 0.0f;
	private float minY = 0.0f;
	private float maxX = 0.0f;
	private float maxY = 0.0f;

	public bool gamePaused;

	void Start() {
		gamePaused = false;
		players = PlayerManager.GetPlayers();
	}

	void LateUpdate () {
		setMinMaxPositions();
		setCameraPosition();
		setCameraScale();
	}

	public void pauseGame(){
		gamePaused = !gamePaused;
		if(gamePaused){
			Time.timeScale = 0.0f;
			foreach(Transform child in transform){
				if(child.gameObject.tag == "HelpScreen"){
					child.gameObject.GetComponent<SpriteRenderer>().enabled = true;
				}
			}
		}
		else{
			Time.timeScale = 1.0f;
			foreach(Transform child in transform){
				if(child.gameObject.tag == "HelpScreen"){
					child.gameObject.GetComponent<SpriteRenderer>().enabled = false;
				}
			}
		}

	}

	// Cycle through the players and find the extreme position values
	private void setMinMaxPositions() {
		// Initialize new calculation with first players position;
		GameObject[] playersArray = players.ToArray();
		if( players.Count > 0 ) {
			minX = playersArray[0].transform.position.x;
			minY = playersArray[0].transform.position.y;
			maxX = playersArray[0].transform.position.x;
			maxY = playersArray[0].transform.position.y;
		}

		// Find the x and y extreme positions of all players
		foreach( GameObject player in players ) {
			minX = Mathf.Min( player.transform.position.x, minX );
			maxX = Mathf.Max( player.transform.position.x, maxX );
			minY = Mathf.Min( player.transform.position.y, minY );
			maxY = Mathf.Max( player.transform.position.y, maxY );
		}
	}

	private void setCameraPosition() {
		// Move the camera to the midpoint of the bounding box of the players
		transform.position = new Vector3((minX + maxX)/2, (minY + maxY)/2, CAMERA_DEPTH);
	}

	float width, height, orthoSize;
	private void setCameraScale() {
		width = maxX - minX;
		height = maxY - minY;
		
		/* set the size to the largest distance between two players and limit camera size to
		   specific range */
		orthoSize = Mathf.Max( capSize( height * 0.5f ) + EDGE_BUFFER, 
		        capSize( width * Screen.height / Screen.width * 0.5f ) + EDGE_BUFFER );

		camera.orthographicSize = orthoSize;
	}

	private float capSize( float size ) {
		float cappedSize = size;
		// Limit camera size
		if( cappedSize < 6 ) {
			cappedSize = 6;
		}
		else if( cappedSize > 10 ) {
			cappedSize = 10;
		}

		return cappedSize;
	}
}
