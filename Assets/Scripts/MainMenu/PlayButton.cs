﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class PlayButton : MonoBehaviour {
	private int numPlayers = 0;
	private GamePadState[] gamePadState;
	// Use this for initialization
	void Start () {
		foreach( string joystick in Input.GetJoystickNames() ) {
			// make sure device is a controller
			if( joystick.Contains( "Controller" ) ) {
				numPlayers++;
			}
			gamePadState = new GamePadState[ numPlayers ];
		}
	}
	
	// Update is called once per frame
	void Update () {
		for( int i = 0; i < numPlayers; i++ ) {
			gamePadState[i] = GamePad.GetState( (PlayerIndex)i );
			if( gamePadState[i].Buttons.A == ButtonState.Pressed ) {
				Application.LoadLevel( "Arena" );
			}
		}
	}
}
