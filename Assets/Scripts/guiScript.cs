﻿using UnityEngine;
using System.Collections;

public class guiScript : MonoBehaviour {

	void OnGUI(){
		// Make a background box
		GUI.Box (new Rect (10,10,100,90), "Start Menu");
		
		// Make the first button. If it is pressed, Application.Loadlevel (1) will be executed
		if (GUI.Button (new Rect (20,40,80,20), "Arena")) {
			Application.LoadLevel ("Arena");
		}
		
		// Make the second button.
		if (GUI.Button (new Rect (20,70,80,20), "Controls")) {
			// show controls window (TODO)
		}
	}
}
