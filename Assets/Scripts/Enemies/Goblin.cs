using UnityEngine;
using System.Collections.Generic;

public class Goblin : MonoBehaviour, Enemy {
	private const int HIT_FORCE = 3500;
	
	public float Speed;
	public bool inTorchRange;
	public const float fearRange = 25.0f;
	
	public PlayerManager pm;
	private List<GameObject> players;
	public Health health = new Health( 30 );
	
	private float distToPlayer1;
	private float distToPlayer2;
	private float distToPlayer3;
	private float distToPlayer4;
	
	public GameObject objectToBeScaredOf;
	public GameObject targetPlayer;
	private	Animator anim; 
	private bool canAttack;

	public AudioClip growl1;
	public AudioClip growl2;
	public AudioClip growl3;
	public AudioClip laugh;
	public AudioClip scream;
	public AudioClip talking;
	public AudioClip shieldHit1;
	public AudioClip shieldHit2;
	public AudioClip shieldHit3;

	
	void Start() {
		canAttack = true;
		anim = GetComponent<Animator> ();
		GameObject pmObject = GameObject.Find ("PlayerManager");
		pm = pmObject.GetComponent<PlayerManager>();
		targetPlayer = pm.GetClosestPlayer (gameObject.transform.position);
		inTorchRange = false;

	}

	float mySpeedX;
	float mySpeedY;
	void Update() {
		mySpeedX = gameObject.rigidbody2D.velocity.x;
		mySpeedY = gameObject.rigidbody2D.velocity.y;
		if (mySpeedX < 0.01f && mySpeedY < 0.01f)
			anim.SetBool ("moving", false);
		else
			anim.SetBool ("moving",true);
	}

	public void Attack()
	{
		if (canAttack)
		{
			anim.SetTrigger("attack");
			canAttack = false;
			Invoke ("EndAttackCooldown", 1f);
		}
	}
	
	float rotation;
	public void SetFacingPlayer() {
		/* Get rotation we need to be set to to be facing the player.  We can do this with
		   ours and the players transforms */
		rotation = Mathf.Atan2 (targetPlayer.transform.position.y - transform.position.y,
		                              targetPlayer.transform.position.x - transform.position.x) * Mathf.Rad2Deg + 90;
		transform.eulerAngles = new Vector3 (0, 0, rotation);
		
	}
	
	public void SetFacingAwayFromPlayer() {
		/* Get rotation we need to be set to to be facing away from the player.  We can do this with
		   ours and the players transforms */
		if (targetPlayer != null){
			rotation = Mathf.Atan2 (transform.position.y  - targetPlayer.transform.position.y,
			                              transform.position.x - targetPlayer.transform.position.x ) * Mathf.Rad2Deg + 90;
		transform.eulerAngles = new Vector3 (0, 0, rotation);
		}
		
	}

	public void SetFacingPoint(Vector3 point) {
		/* Get rotation we need to be set to to be facing the player.  We can do this with
		   ours and the players transforms */
		rotation = Mathf.Atan2 (point.y - transform.position.y,
		                              point.x - transform.position.x) * Mathf.Rad2Deg + 90;
		transform.eulerAngles = new Vector3 (0, 0, rotation);
		
	}
	
	public void Move() {
		rigidbody2D.AddForce( gameObject.transform.up * -Speed );
	}
	
	public void Move(float frac){
		rigidbody2D.AddForce( gameObject.transform.up * -Speed * frac );
	}
	
	void Enemy.Hit( Collider2D col ) {
		rand = Random.Range(0.0f,10.0f);
		if(rand >= 5.0f){
			audio.clip = growl1;
		}
		else{
			audio.clip = growl2;
		}
		audio.Play();
		health.DecreaseHealth( 10 );
		if( health.GetHealth() <= 0 ) {
			Destroy( this.gameObject );
		}
		FlyBack( col );
	}
	
	Vector2 back, tmp;
	float angle;
	private void FlyBack( Collider2D col ) {
		// Get the direction away from the player.  We will add a force in this direction
		angle = col.transform.parent.transform.eulerAngles.z;
		tmp = PlayerTargeting.angleToVec2 (angle);
		rigidbody2D.AddForce( tmp*-HIT_FORCE );
	}
	
	float rand = 0.0f, timer = 0.0f;
	void OnCollisionEnter2D(Collision2D col) {
		if (col.collider.tag == "Shield" && Time.time - timer > 3.0f){
			timer = Time.time;
			rand = Random.Range(0.0f,10.0f);
			if(rand >= 6.66f){
				audio.clip = shieldHit1;
			}
			else if(rand >= 3.33f){
				audio.clip = shieldHit2;
			}
			else{
				audio.clip = shieldHit3;
			}
			audio.Play();
		}
	}

	private void EndAttackCooldown()
	{
		canAttack = true;
	}
}
