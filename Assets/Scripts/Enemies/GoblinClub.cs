﻿using UnityEngine;
using System.Collections;

public class GoblinClub : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	} 

	void OnCollisionEnter2D(Collision2D col) {
		if( col.collider.tag == "PlayerChest" ) {
			col.collider.gameObject.GetComponent<PlayerBody>().DecreaseHealth( 10 );
		}
	}
}
