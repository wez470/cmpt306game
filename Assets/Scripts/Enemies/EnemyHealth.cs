﻿using UnityEngine;
using System.Collections;

public class EnemyHealth : MonoBehaviour {
	public SpriteRenderer HealthBar;

	private int health = 100;
	private Vector3 healthScale;

	void Start() {
		// Getting the intial scale of the healthbar (whilst the enemy has full health).
		healthScale = HealthBar.transform.localScale;
	}

	void Update() {
		DisplayHealthBar();
	}

	private void DisplayHealthBar() {
		// Set the health bar's colour to proportion of the way between green and red based on the enemies's health.
		HealthBar.material.color = Color.Lerp(Color.green, Color.yellow, 1 - health * 0.01f);
		
		// Set the scale of the health bar to be proportional to the enemies's health.
		HealthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, 1, 1);
	}

	public void DecreaseHealth( int amount ) {
		health -= amount;
	}

	public int GetHealth() {
		return health;
	}

	public void SetMaxHealth( int maxHealth ) {
		health = maxHealth;
	}
}
