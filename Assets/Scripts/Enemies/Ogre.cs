﻿using UnityEngine;
using System.Collections;

public class Ogre : MonoBehaviour, Enemy {
	private const int HIT_FORCE = 1000; 

	public float Speed; 
	public GameObject targetPlayer;
	private float distToPlayer1;
	private	Animator anim; 
	public EnemyHealth health;
	public PlayerManager pm;

	public AudioClip growl1;
	public AudioClip growl2;
	public AudioClip growl3;
	public AudioClip growl4;
	public AudioClip growl5;
	public AudioClip growl7;
	public AudioClip shieldHit1;
	public AudioClip shieldHit2;
	public AudioClip shieldHit3;

	// For now just target player1
	void Awake() 
	{
		health = GetComponent<EnemyHealth>();
	}

	void Start() {
		GameObject pmObject = GameObject.Find ("PlayerManager");
		pm = pmObject.GetComponent<PlayerManager>();
		anim = GetComponent<Animator>(); 
		health.SetMaxHealth( 150 );
		targetPlayer = pm.GetClosestPlayer (gameObject.transform.position);	
	}

	void FixedUpdate() {

		}

	public void Attack()
	{
		anim.SetTrigger("attack");
	}

	public void SetFacingPlayer() {
		/* Get rotation we need to be set to to be facing the player.  We can do this with
		   ours and the players transforms */
		float rotation = Mathf.Atan2( targetPlayer.transform.position.y - transform.position.y,
		                             targetPlayer.transform.position.x - transform.position.x ) * Mathf.Rad2Deg - 270;
		transform.eulerAngles = new Vector3( 0, 0, rotation );
	}

	public void SetFacingPoint(Vector3 point) {
		/* Get rotation we need to be set to to be facing the player.  We can do this with
		   ours and the players transforms */
		float rotation = Mathf.Atan2 (point.y - transform.position.y,
		                              point.x - transform.position.x) * Mathf.Rad2Deg + 90;
		transform.eulerAngles = new Vector3 (0, 0, rotation);
		
	}


	public void MoveTowardsPlayer() {
		rigidbody2D.AddForce( gameObject.transform.up * -Speed );
	}

	void Enemy.Hit( Collider2D col ) {
		rand = Random.Range(0.0f,10.0f);
		if(rand >= 6.66f){
			audio.clip = growl2;
		}
		else if(rand >= 3.33f){
			audio.clip = growl4;
		}
		else{
			audio.clip = growl7;
		}
		audio.Play();

		health.DecreaseHealth( 10 );
		if( health.GetHealth() <= 0 ) {
			Destroy( this.gameObject );
		}
		FlyBack( col );
	}
	
	private void FlyBack( Collider2D col ) {
		// Get the direction away from the collision.  We will add a force in this direction
		float backRotation = Mathf.Atan2(col.transform.position.y  - transform.position.y,
		        col.transform.position.x - transform.position.x ) * Mathf.Rad2Deg + 90;
		Vector2 back = new Vector2( Mathf.Cos( backRotation * Mathf.Deg2Rad ), 
		        -Mathf.Sin( backRotation * Mathf.Deg2Rad ) ) * HIT_FORCE;
		rigidbody2D.AddForce( back );
	}

	float rand = 0.0f, timer = 0.0f;
	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.tag == "PlayerChest" || col.gameObject.tag == "Player1" || col.gameObject.tag == "Player2" || col.gameObject.tag == "Player3" || col.gameObject.tag == "Player4" || col.gameObject.tag == "Shield") {
			col.gameObject.rigidbody2D.AddForce(PlayerTargeting.angleToVec2 (col.gameObject.transform.eulerAngles.z)*HIT_FORCE*15);
			 if (col.collider.tag == "Shield" && Time.time - timer > 1.0f){
				timer = Time.time;
				rand = Random.Range(0.0f,10.0f);
				if(rand >= 6.66f){
					audio.clip = shieldHit1;
				}
				else if(rand >= 3.33f){
					audio.clip = shieldHit2;
				}
				else{
					audio.clip = shieldHit3;
				}
				audio.Play();
			}
		}
	}
		
	void EnableHandColliders() {
		foreach( OgreFists ogreFistScript in GetComponentsInChildren<OgreFists>() ) {
			ogreFistScript.EnableHandColliders();
		}
	}
}
