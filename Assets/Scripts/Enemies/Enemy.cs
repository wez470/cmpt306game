﻿using UnityEngine;

public interface Enemy {
	void Hit( Collider2D col );
}
