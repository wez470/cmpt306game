﻿using UnityEngine;
using System.Collections;

public class OgreFists : MonoBehaviour {
	Animator anim;
	public GameObject LeftHand; 
	public GameObject RightHand; 
	
	// Use this for initialization
	void Start () {
		anim = GetComponentInParent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D col) {
		if (col.gameObject.tag == "PlayerChest" || col.gameObject.tag == "Player1" || col.gameObject.tag == "Player2" || col.gameObject.tag == "Player3" || col.gameObject.tag == "Player4" || col.gameObject.tag == "Shield") {
			anim.SetTrigger("BreakAttack");
			LeftHand.GetComponent<CircleCollider2D>().enabled = false;
			RightHand.GetComponent<CircleCollider2D>().enabled = false;
			Invoke ("EnableHandColliders", .5f);
			if( col.collider.gameObject.tag == "PlayerChest") {
				col.collider.gameObject.GetComponent<PlayerBody>().DecreaseHealth( 20 );
			}
		}
	}

	public void EnableHandColliders() {
		LeftHand.GetComponent<CircleCollider2D>().enabled = true;
		RightHand.GetComponent<CircleCollider2D>().enabled = true;
	}
}
