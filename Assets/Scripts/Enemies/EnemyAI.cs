using UnityEngine;
using System.Collections;
using Pathfinding;

public class EnemyAI : MonoBehaviour
{

	public PlayerManager pManager;
	private string eType;
	private GameObject target;
	

	// Health will be type Health or type EnemeyHealth, depending if ogre or goblin
	private Health gobHealth;
	private EnemyHealth ogreHealth;
	//private Health health = new Health( 30 );
	
	private float distToPlayer1;
	private float distToPlayer2;
	private float distToPlayer3;
	private float distToPlayer4;
	
	private GameObject player1;
	private GameObject player2;
	private GameObject player3;
	private GameObject player4;
	
	private GameObject torch1;
	private GameObject torch2;
	private GameObject torch3;
	private GameObject torch4;
	
	private float attackRange;

	// Goblin specific parameters
	private bool inTorchRad;
	public const float fearRange = 25.0f;
	int fleeCount = 0;
	
	// will be type Ogre or type Goblin
	private Ogre ogreScript;
	private Goblin gobScript;
	private int walkSteps;
	private int idleSteps;
	
	//Pathfinding stuff
	private Seeker seeker;
	public Path path;
	private float nextWaypointDistance = 3f;
	private int currentWaypoint = 0;
	private float GetNewPath = 2.0f;
	private float lastTime;
	private float lastTargetTime;
	private float targetCD = 2.0f;
	public LayerMask EnemyScan;

	delegate void Delegate();
	Delegate action;
	
		// Use this for initialization
	void Start ()
	{
		GameObject pmObject  = GameObject.FindGameObjectWithTag("PlayerManager");
		pManager = pmObject.GetComponent<PlayerManager>();
		action = TargetVisible;
		// Find references to each players
		player1 = GameObject.FindGameObjectWithTag( "Player1" );
		player2 = GameObject.FindGameObjectWithTag( "Player2" );
		player3 = GameObject.FindGameObjectWithTag( "Player3" );
		player4 = GameObject.FindGameObjectWithTag( "Player4" );
		lastTime = -0.5f;
		lastTargetTime = -2.0f;
		
		inTorchRad = false;
		seeker = GetComponent<Seeker>();
		
		eType = gameObject.tag;
		if (eType == "Ogre")
		{
			ogreScript = GetComponent<Ogre>(); 
			ogreHealth = ogreScript.health; 
			attackRange = 5f;
		}
		else if (eType == "Goblin")
		{
			gobScript = GetComponent<Goblin>();
			gobHealth = gobScript.health;
			attackRange = 1.5f;
		}


	}

	// Update is called once per frame
	void FixedUpdate ()
	{
	
		if (lastTime + GetNewPath < Time.time){
			action = TargetVisible;
			lastTime = Time.time;
		}
		// if walkSteps greater than 0, finish random walk
		if(walkSteps > 0)
		{
			walkSteps--;
			if (eType == "Ogre"){
				ogreScript.MoveTowardsPlayer ();
			}
			else{
				gobScript.Move ();
			}
		}
		// if idleStpes greater than 0, sty idle until steps are 0
		else if (idleSteps > 0)
		{
			idleSteps--;
			// do nothing (idle)
		}
		// else we are not idle or on a random walk, get new action
		else
		{
			action ();
		}
	}

	float distToClosestPlayer;
	
	// See if a player is visible 
	RaycastHit2D h;
	
	private void TargetVisible()
	{
		// get closest player
		if (target == null || (targetCD + lastTargetTime < Time.time)){
			target = pManager.GetClosestPlayer (gameObject.transform.position);
			lastTargetTime = Time.time;
		}
		h = Physics2D.Raycast(this.transform.position, target.transform.position-transform.position
					,Vector2.Distance(transform.position,target.transform.position),EnemyScan);
		if (h.collider == null){
			if (eType == "Goblin")
			{
				InTorchRange();
			}
			else
			{
				InRange();
			}
		}
		else
		{
			//Debug.Log (h.collider.gameObject.tag + " is blocking the view of the enemy to the player.");
			seeker.StartPath (transform.position,target.transform.position,OnPathComplete);
			action = followPath;
		}
	}
	
	Vector3 pt;
	private void followPath(){
		
		
		
		if (path == null) {
			//We have no path to move after yet
			return;
		}
		
//		Debug.Log (path.vectorPath[currentWaypoint].x + "," + path.vectorPath[currentWaypoint].y + "," + "," + path.vectorPath[currentWaypoint].z +
//		           " to " + path.vectorPath[currentWaypoint+1].x + "," + path.vectorPath[currentWaypoint+1].y + "," + "," + path.vectorPath[currentWaypoint+1].z);
		
		if (currentWaypoint >= path.vectorPath.Count) {
			//Debug.Log ("End Of Path Reached");
			currentWaypoint = 0;
			action = TargetVisible;
			return;
		}
		
		//Direction to the next waypoint
		pt = (path.vectorPath[currentWaypoint]);
		
		if (eType == "Goblin"){
			gobScript.SetFacingPoint (pt);
			gobScript.Move();
		}
		else
		{
			ogreScript.SetFacingPoint (pt);
			ogreScript.MoveTowardsPlayer();
		}
		
		//Check if we are close enough to the next waypoint
		//If we are, proceed to follow the next waypoint
		if (Vector2.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
	}

	// This method can only be entered by a goblin (Ogre does not have torch)
	private void InTorchRange()
	{
		// If goblin is of distance 10 away from a player we are not in torch range
		// Find the closest player whose torch is active, and set Goblin to be scared of them
		inTorchRad = false; 
		gobScript.objectToBeScaredOf = null;
		if(player1 != null && player1.GetComponent<Player>().myTorch != null){
			torch1 = player1.GetComponent<Player>().myTorch;
			distToPlayer1 = Vector3.Distance (gameObject.transform.position, player1.transform.position);
			if (distToPlayer1 <= fearRange && torch1 != null) {
				if (torch1.GetComponent<CircleCollider2D>().enabled == true)
				{
					inTorchRad = true;
					gobScript.targetPlayer = player1;
					target = player1;
				}
			}
		}
		if(player2 != null && player2.GetComponent<Player>().myTorch != null){
			torch2 = player2.GetComponent<Player>().myTorch;
			distToPlayer2 = Vector3.Distance (gameObject.transform.position, player2.transform.position);
			if (distToPlayer2 <= fearRange && torch2 != null) {
				if (torch2.GetComponent<CircleCollider2D>().enabled == true)
				{
					inTorchRad = true;
					gobScript.targetPlayer = player2;
					target = player2;
				}
			}
		}
		if(player3 != null && player3.GetComponent<Player>().myTorch != null){
			torch3 = player3.GetComponent<Player>().myTorch;
			distToPlayer3 = Vector3.Distance (gameObject.transform.position, player3.transform.position);
			if (distToPlayer3 <= fearRange && torch3 != null) {
				if (torch3.GetComponent<CircleCollider2D>().enabled == true)
				{
					inTorchRad = true;
					gobScript.targetPlayer = player3;
					target = player3;
				}
			}
		}
		if(player4 != null && player4.GetComponent<Player>().myTorch != null){
			torch4 = player4.GetComponent<Player>().myTorch;
			distToPlayer4 = Vector3.Distance (gameObject.transform.position, player4.transform.position);
			if (distToPlayer4 <= fearRange && torch4 != null) {
				if (torch4.GetComponent<CircleCollider2D>().enabled == true)
				{
					inTorchRad = true;
					gobScript.targetPlayer = player4;
					target = player4;
				}
			}
		}

		if (inTorchRad)
		{
			Flee();
		}
		else
		{
			InRange();
		}
	}

	// Check if in attack range
	private void InRange()
	{
		// See if in range of closest enemy
		if (target == null || (targetCD + lastTargetTime < Time.time)){
			target = pManager.GetClosestPlayer (gameObject.transform.position);
			lastTargetTime = Time.time;
		}
		distToClosestPlayer = Vector3.Distance (target.transform.position, transform.position);
		// check if zombie is in attack range 
		if (distToClosestPlayer <= attackRange)
		{
			NearDeath();
		}
		else
		{
			HpNotLow();
		}
	}
	
	int coinFlip;
    // Randomly generate a number between 1 and 2 (50% chance to random walk, 50% chance to idle)
	private void RandNum() 
	{
		coinFlip = Random.Range (1, 3);
		if (coinFlip == 1)
		{
			action = RandWalk;
		}
		else
		{
			action = Idle;
		}
	}

	// Check if player is near death
	private void NearDeath()
	{
		if (eType == "Ogre")
		{
			Attack();
		}
		else
		{
			if (gobHealth.GetHealth() <= 10)
			{
				Flee();
			}
			else
			{
				Attack();
			}
		}
	}

	// Check if player is still in condition to fight
	private void HpNotLow()
	{
		if (eType == "Ogre")
		{
			if (ogreHealth.GetHealth() > 20) 
			{
				Advance();
			}
			else 
			{
				Flee();
			}
		}
		else
		{
			if (gobHealth.GetHealth () > 10)
			{
				Advance();
			}
			else
			{
				Flee();
			}
		}
	}
	
	// run away from player
	private void Flee()
	{
		
		
		//Debug.Log ("Fleeing");
		if (eType == "Ogre")
		{
			// Ogres Dont Flee
		}
		else
		{
			if (fleeCount > 500){
			 	gobScript.health.IncreaseHealth(1);
			 	fleeCount = 0;
			 }
			fleeCount++;
			gobScript.targetPlayer = target;
			gobScript.SetFacingAwayFromPlayer();
			gobScript.Move (0.5f);
			action = TargetVisible;
		}
		action = TargetVisible;
	}
	
	// attack player 
	private void Attack()
	{
		//Debug.Log ("Attacking");
		if (eType == "Ogre")
		{
			ogreScript.Attack();
		}
		else
		{
			gobScript.Attack();
		}
		action = TargetVisible;
	}
	
	// move towards player
	private void Advance()
	{
		//Debug.Log ("Advancing");
		if (eType == "Ogre")
		{
			ogreScript.targetPlayer = target;
			ogreScript.SetFacingPlayer();
			ogreScript.MoveTowardsPlayer ();
		}
		else
		{
			gobScript.targetPlayer = target;
			gobScript.SetFacingPlayer();
			gobScript.Move ();
		}
		action = TargetVisible;
	}
	
	public void OnPathComplete (Path p) {
		//Debug.Log ("Yay, we got a path back. Did it have an error? "+p.error);
		if (!p.error) {
			path = p;
//			for (int i = 0; i < p.vectorPath.Count; i++){
//				Debug.Log (p.vectorPath[i].x + "," + p.vectorPath[i].y + "," + p.vectorPath[i].z);
//			}
			//Debug.Log ("Last node is: " + p.vectorPath[p.vectorPath.Count-1].x + "," + p.vectorPath[p.vectorPath.Count-1].y + "," + p.vectorPath[p.vectorPath.Count-1].z);
			//Debug.Log (path.DebugString((PathLog)3));
			//Reset the waypoint counter
			currentWaypoint = 0;
		}
	}
	
	Vector2 randPoint;
	// Randomly walk
	private void RandWalk()
	{

		//Debug.Log ("Random Walk");
		// randomly choose a point within radius 10 of goblin
		randPoint = Random.insideUnitCircle * 10;

		if (eType == "Goblin")
		{
			//gobScript.targetPlayer = target;
			gobScript.SetFacingPoint(new Vector3(randPoint.x, randPoint.y, 1));
		}
		else
		{
			ogreScript.SetFacingPoint(new Vector3(randPoint.x, randPoint.y, 1));
		}

		walkSteps = 10;

		action = TargetVisible;
	}

	// Set to be idle
	private void Idle()
	{
		idleSteps = 40;
		// Do nothing
		action = TargetVisible;
	}
	
	public void OnDisable () {
		//seeker.pathCallback -= OnPathComplete;
	}
		

}

