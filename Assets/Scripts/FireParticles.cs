﻿using UnityEngine;
using System.Collections;

public class FireParticles : MonoBehaviour {

	// Use this for initialization
	void Start () {
		renderer.sortingLayerName = "Default";
		renderer.sortingOrder = 12;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void DestroyAllParticles() {
		particleEmitter.ClearParticles();
	}
}
