using UnityEngine;
using System.Collections;

public class Health {
	private readonly int maxHealth;
	private int health;
	
	public Health( int maxHealth ) {
		this.maxHealth = maxHealth;
		health = maxHealth;
	}
	
	public void IncreaseHealth( int amount ) {
		health += amount;
	}
	
	public void DecreaseHealth( int amount ) {
		health -= amount;
	}
	
	public int GetHealth() {
		return health;
	}
	
	public int GetMaxHealth() {
		return maxHealth;
	}
	
	public void ResetHealth() {
		health = maxHealth;
	}
}
