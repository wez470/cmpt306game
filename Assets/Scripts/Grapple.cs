﻿using UnityEngine;
using System.Collections;

public class Grapple : MonoBehaviour {
	public GameObject Rope;

	private bool attached;
	private GameObject playerWhoShot;
	private GameObject playerLeftHand;
	private GameObject currRope;
	private const float ROPE_DESTROY_LENGTH = 25.0f;
	private Rope rope;

	public AudioClip grappleStart;
	public AudioClip grappleRope;
	public AudioClip grappleHit;

	// Use this for initialization
	void Start () {
		audio.clip = grappleRope;
		audio.loop = true;
		audio.Play();
		attached = false;
		Physics2D.IgnoreLayerCollision( LayerMask.NameToLayer( "Grapple" ), LayerMask.NameToLayer( "Rope" ), true );
		currRope = Instantiate( Rope, transform.position, transform.rotation ) as GameObject;
		rope = currRope.GetComponent<Rope>();
		rope.SetPlayerAndGrapple( playerLeftHand.transform, transform );
	}

	// Reference to which player shot this grapple
	public void setWhoFired(GameObject whoFired){
		playerWhoShot = whoFired;
		playerLeftHand = whoFired.GetComponent<Player>().LeftHand;
	}

	void Update() {
		if(attached){
			rigidbody2D.velocity = new Vector2(0.0f, 0.0f);
		}
		if (rope.distPlayerToGrapple > ROPE_DESTROY_LENGTH){
			KillGrapple();
		}
	}
	
	// If you leave the screen delete yourself
	void KillGrapple(){
		Destroy( this.gameObject );
		Destroy( currRope );
		if (playerWhoShot.GetComponent<Player>().myGrapple == null)
			playerWhoShot.GetComponent<Player>().makeGrapple();

	}

	// Attach to anything that isen't the player who shot this
	// Delete yourself if the grapple collides with the player who shot this
	void OnCollisionEnter2D(Collision2D col) {
		audio.loop = false;
		if(col.gameObject.tag == playerWhoShot.tag){
			Destroy(this.gameObject);
			Destroy(currRope);
			if (playerWhoShot.GetComponent<Player>().myGrapple == null)
				playerWhoShot.GetComponent<Player>().makeGrapple();

		}
		else {
			attached = true;
		}
	}

	public bool isAttached(){
		return attached;
	}

	void OnDestroy() {
		Destroy( currRope );
	}
}
