﻿using UnityEngine;
using System.Collections;

public class WaveManager : MonoBehaviour {
	private const int NAME_TOP = 60;
	private const int WIDTH = 200;
	private const int NAME_HEIGHT = 30;
	private const int BUFFER = 50;

	public EnemySpawnController enemySpawner;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnGUI() {
		GUIStyle style = new GUIStyle();
		style.normal.textColor = Color.black;
		style.fontSize = 30;
		style.fontStyle = FontStyle.Bold;
		string name = "Wave: " + enemySpawner.Wave;
		GUI.Label(new Rect( BUFFER, NAME_TOP, WIDTH, NAME_HEIGHT ), name, style);
	}
}
