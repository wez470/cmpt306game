﻿using UnityEngine;
using System.Collections;
using XInputDotNetPure;

public class Sword : MonoBehaviour {

	public int sNum;
	public float rmblCoolDown = 0.5f;
	private float lastRumble;

	public AudioClip swordhit1;
	public AudioClip swordhit2;
	public AudioClip swordhit3;
	
	void Awake(){
		lastRumble = 0.0f;
	}

	IEnumerator rumble(){
		for (int i = 100; i >= 0; i--){
			for (int j = 0; j < 10; j++){
				GamePad.SetVibration((PlayerIndex)sNum, i/100, i/100);
				yield return null;
			}
				
		}
		yield return null;
	}
	
	float rand = 0.0f;
	// Resolve sword collision
	void OnTriggerEnter2D(Collider2D col) {
		if(col.gameObject.tag == "Goblin" || col.gameObject.tag == "Ogre") {
			( (Enemy)col.gameObject.GetComponent( typeof( Enemy ) ) ).Hit( this.collider2D );
			GetComponent<BoxCollider2D>().enabled = false;
			if (Time.time - lastRumble > rmblCoolDown){
				lastRumble = Time.time;
				sNum = GetComponentInParent<Player>().playerNum;
				StartCoroutine("rumble");
			}
			rand = Random.Range(0.0f,10.0f);
			if(rand >= 6.66f){
				audio.clip = swordhit1;
			}
			else if(rand >= 3.33f){
				audio.clip = swordhit2;
			}
			else{
				audio.clip = swordhit3;
			}
			audio.Play();
		}
		
		
	}
}
